package com.chornge.isspass;

import com.chornge.isspass.adapter.ISSAdapter;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Date;

import static org.hamcrest.CoreMatchers.anything;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ISSUnitTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    // Arrange
    @Mock
    private ISSAdapter issAdapter;

    @Test
    public void testGPSOnDevice() throws Exception {
        anything();
    }

    @Test
    public void testTimeStampConverter() throws Exception {

        // Act
        long timeInMillis = 1515733199;
        String formattedDate = "Thu, Jan 11, 2018 10:59 PM";
        when(issAdapter.getHumanReadableDate(new Date(timeInMillis * 1000L))).thenReturn(formattedDate);

        // Assert
        assertEquals(issAdapter.getHumanReadableDate(new Date(1515733199 * 1000L)), formattedDate);
    }
}