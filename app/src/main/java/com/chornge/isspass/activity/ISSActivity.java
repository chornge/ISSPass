package com.chornge.isspass.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chornge.isspass.R;
import com.chornge.isspass.adapter.ISSAdapter;
import com.chornge.isspass.model.ISSResponse;
import com.chornge.isspass.model.Response;
import com.chornge.isspass.network.ISSAPICall;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ISSActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_CODE = 5;

    @BindView(R.id.list_of_passes)
    ListView adapterlist;

    @BindView(R.id.number_of_passes_text)
    TextView passesTextView;

    @BindView(R.id.pull_to_refresh_container)
    SwipeRefreshLayout pullToRefreshLayout;

    String numberOfPasses;
    LocationManager locationManager;
    String latitude;
    String longitude;
    String city = "";
    String state = "";

    List<Response> responseList = null;

    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iss);

        ButterKnife.bind(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //noinspection Convert2MethodRef
        pullToRefreshLayout.setOnRefreshListener(() -> fetchDataFromServer());

        initRetrofit();
        getDeviceCoordinates();
        fetchDataFromServer();
        findLocalAddressFromCoordinates(latitude, longitude);
    }

    /**
     * Build the retrofit object.
     */
    private void initRetrofit() {
        String baseUrl = getResources().getString(R.string.app_base_url);
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    /**
     * Access the latitude and longitude information from the device (if available).
     */
    private void getDeviceCoordinates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                longitude = String.valueOf(location.getLongitude());
                latitude = String.valueOf(location.getLatitude());
            }
        }
    }

    /**
     * Make a call to the ISS-Open-Notify server.
     */
    private void fetchDataFromServer() {
        ISSAPICall passTimeAPI = retrofit.create(ISSAPICall.class);
        Call<ISSResponse> call = passTimeAPI.getPassTimes(latitude, longitude);
        call.enqueue(new Callback<ISSResponse>() {
            @Override
            public void onResponse(@NonNull Call<ISSResponse> call,
                                   @NonNull retrofit2.Response<ISSResponse> response) {

                // To clear any old items before appending items to the list:
                if (responseList != null) {
                    responseList.clear();
                }

                //noinspection ConstantConditions
                responseList = response.body().getResponse();

                ISSAdapter adapter = new ISSAdapter(getApplicationContext(),
                        R.layout.list_view, responseList);
                adapterlist.setAdapter(adapter);

                //noinspection StringBufferReplaceableByString
                StringBuilder stringBuilder = new StringBuilder();

                //noinspection ConstantConditions
                numberOfPasses = String.valueOf(responseList.size());

                //noinspection ConstantConditions
                stringBuilder.append("The Space Station will make ")
                        .append(numberOfPasses)
                        .append(" ")
                        .append(getResources().getQuantityString(R.plurals.passes,
                                Integer.valueOf(numberOfPasses)))
                        .append(" over (")
                        .append(latitude)
                        .append(" , ")
                        .append(longitude)
                        .append(") - ")
                        .append(city)
                        .append(", ")
                        .append(state);

                passesTextView.setText(stringBuilder.toString());
                pullToRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(@NonNull Call<ISSResponse> call, @NonNull Throwable t) {
                if (responseList != null) {
                    responseList.clear();
                }

                Toast toast = Toast.makeText(ISSActivity.this,
                        getResources().getString(R.string.error_message), Toast.LENGTH_SHORT);

                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                pullToRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Find the city and state information of the coordinates obtained from device's coordinates.
     */
    private void findLocalAddressFromCoordinates(String lat, String lon) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.valueOf(lat), Double.valueOf(lon), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null) {
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                getDeviceCoordinates();
                break;

            default:
                break;
        }
    }
}
