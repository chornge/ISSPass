package com.chornge.isspass.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request {

    @SerializedName("altitude")
    @Expose
    private int altitude;

    @SerializedName("datetime")
    @Expose
    private int datetime;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("passes")
    @Expose
    private int passes;

    public int getPasses() {
        return passes;
    }
}