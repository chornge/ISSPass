package com.chornge.isspass.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("duration")
    @Expose
    private int duration;

    @SerializedName("risetime")
    @Expose
    private int risetime;

    public int getDuration() {
        return duration;
    }

    public int getRisetime() {
        return risetime;
    }
}
