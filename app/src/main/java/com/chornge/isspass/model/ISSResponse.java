package com.chornge.isspass.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ISSResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("request")
    @Expose
    private Request request;

    @SerializedName("response")
    @Expose
    private List<Response> response = null;

    public List<Response> getResponse() {
        return response;
    }
}
