package com.chornge.isspass.network;

import com.chornge.isspass.model.ISSResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ISSAPICall {
    @GET("/iss-pass.json")
    Call<ISSResponse> getPassTimes(@Query("lat") String latitude, @Query("lon") String longitude);
}
