package com.chornge.isspass.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chornge.isspass.R;
import com.chornge.isspass.model.Response;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ISSAdapter extends ArrayAdapter {

    @BindView(R.id.duration)
    TextView duration;

    @BindView(R.id.timestamp)
    TextView timestamp;

    public ISSAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        //noinspection unchecked
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_view, parent, false);
            ButterKnife.bind(this, view);
        }

        Response singlePass = (Response) getItem(position);

        if (singlePass != null) {
            long unixSeconds = singlePass.getRisetime();
            String formattedDate = getHumanReadableDate(new Date(unixSeconds * 1000L));

            long durationInLong = singlePass.getDuration() * 1000L;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(durationInLong);

            //noinspection StringBufferReplaceableByString
            StringBuilder durationTextBuilder = new StringBuilder();
            durationTextBuilder.append("Pass #")
                    .append(position + 1)
                    .append(" will be over your location for ")
                    .append(singlePass.getDuration())
                    .append(" seconds")
                    .append(" (Approx ")
                    .append(minutes)
                    .append(" mins)");
            duration.setText(durationTextBuilder.toString());

            //noinspection StringBufferReplaceableByString
            StringBuilder timestampTextBuilder = new StringBuilder();
            timestampTextBuilder.append("On ")
                    .append(formattedDate);
            timestamp.setText(timestampTextBuilder.toString());
        }

        return view;
    }

    /**
     * Convert date in millis to a more readable format.
     */
    public String getHumanReadableDate(Date dateInMillis) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm aa", Locale.US);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(dateInMillis);
    }
}
